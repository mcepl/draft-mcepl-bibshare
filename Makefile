all: bibshare.html bibshare.xml bibshare.txt

bibshare.html: body.xml
	xml2rfc template.xml -f $@ --html

bibshare.xml: body.xml
	xml2rfc template.xml -f $@ --exp

bibshare.nroff: body.xml
	xml2rfc template.xml -f $@ --nroff

bibshare.txt: body.xml
	xml2rfc template.xml -f $@ --text

body.xml: body.md bib/reference.RFC.2119.xml transform.xsl template.xml
	pandoc -s -f markdown+definition_lists -t docbook $< \
	|xsltproc --nonet transform.xsl - >$@

clean:
	rm -f bibshare.{xml,txt,nroff,html} body.xml *~
