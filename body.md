Introduction
============

This is a text.

http://blog.martinfenner.org/2013/06/19/citations-in-scholarly-markdown/

Requirements Language
=====================

The key words "MUST", "MUST NOT", "REQUIRED", "SHALL", "SHALL NOT",
"SHOULD", "SHOULD NOT", "RECOMMENDED", "MAY", and "OPTIONAL" in this
document are to be interpreted as described in [RFC2119].

In this document, these words will appear with that interpretation only
when in UPPER CASE.  Lower case uses of these words are not to be
interpreted as carrying [RFC2119] significance.


Construction of citation keys:
==============================

@article

:    Author:&lt;abbrev-journal>-&lt;volume or yyear>-&lt;start page or month#>


@inproceedings

:    Author:&lt;abbrev-booktitle>&lt;yyear>-&lt;start page> [no -, e.g, RIDT91]

@incollection

:    Author:&lt;abbrev-booktitle>-&lt;yyear>-&lt;start page>
        
@proceedings, @book, @...thesis

:    Author:&lt;abbrev-title>-&lt;yyear>

@unpublished

:    Author:&lt;abbrev-title>-&lt;yyear>-&lt;month# or whatever>

@techreport

:    Author:&lt;abbrev-title>-&lt;yyear> or

:    Author:&lt;abbrev-inst>-&lt;yyear>-&lt;number>


To make abbrev, use initial letters from the first three or so important
words in the title.  If title has one word, probably better to
abbreviate it than to use just a single letter, as in `SCI' rather than
`S' for the journal `Science'.  For books it probably doesn't matter much.


Use \\#, \\&, \\$, \\allcaps

journal string prefix

:    j-                     

publisher	           

:    pub-                  

publisher address          

:    pub-&lt;publisher>:adr

institution

:    inst-               


This book has a good collections of journal abbreviations:

* Coden for Periodical Titles, Volume 1, ASTM Data Series DS 23A,

* American Society for Testing and Materials, 1916 Race St, Philadelphia, PA 19103


CONVENTIONS (for naming BibTeX citation keys)
=============================================

Books are tagged by the first author's last name, a colon, up to 3
upper-case letters taken from the first three upper-case words in the
title (ignoring words like A, And, The), followed by the last two digits
of the publication year.  If there is a volume entry, it is appended to
the tag, prefixed by a hyphen.

When appropriate, a van part is included in the author tag.  For names
with accented letters, accents are dropped in the author tag.

This scheme is systematic enough that it can be programmed: most of the
Addison-Wesley book entries were created with an awk program from a dump
of the AW database supplied by Mona Zeftel.  Older entries in this
bibliography were modified on 28-Nov-1990 to conform to this tagging
scheme.

The choice of a limit of 3 letters was determined from experiments on
the Addison-Wesley collection.  Long tags are undesirable because they
are a nuisance to type, and also interfere with the tagged bibliography
output produced using the LaTeX showtags style option.

Journal article tags look like author:abbrev-volume-number-page, where
the author part is the last name of the first author: for example,
Gilchrist:NAMS-36-9-1199.

Technical report tags look like author:abbrev-number: for example,
Billawala:STAN-CS-89-1256.

InProceedings tags look like author:conferencename-page: for example,
Agostini:TEX85-117.

With few exceptions, value fields for acknowledgement, address, journal,
and publisher keywords should use macros.  This helps to ensure
consistency, and reduces the entry sizes.  Address entries must always
include the country.



Security Considerations
=======================

This RFC raises no new security issues.

Acknowledgments
===============

This memo is mostly based on the file bibshare, which used to be part of
some BibTeX distributions as $TEXMF/doc/bibtex/base/bibshare by Karl
Berry &lt;http://thread.gmane.org/gmane.comp.tex.texhax/19719/focus=19722>
allegedly based on work of Nelson Beebe.
